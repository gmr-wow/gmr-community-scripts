# GMR Community Scripts

Welcome to the official GMR repository.
You will find all plugins and profiles free and developed by the community, for the community

## Contribute

You can contribute to the project by creating a branch.  
 Once your development is complete, create a merge request and your work will be merged to the main branch if approved

## Contribution Rules

Please use underscore case as naming convention of your files.  
Some examples:
 * mage_frost_aoe_rotation.lua
 * questing_kalimdor_1_to_80.lua

# API Documentation
- [GMR API Documentation (incomplete)](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/api/_gmr_definitions.lua)

# Available Scripts
## Battleground | Scripts / Plugins
##### Alterac Valley
- [Mons | Alterac Valley Plugin](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/plugins/wotlk/battlegrounds/mons_alterac_valley.lua)
## Class Rotations / Plugins
##### DeathKnight
- [AmsTaFFix | Death Knight Blood](https://github.com/AmsTaFFix/gmr-stuff/tree/main/plugins/wotlk/rotations/deathknight)
##### Druid
- [Easy | Moonkin Eclipse Starfall](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/druid/easy_moonkin_eclipse_starfall.lua)
##### Hunter
- [Extacy | Chimera-Shot HotFix](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/hunter/extacy_chimerashot_hotfix.lua)
##### Mage
##### Paladin
- [Lezbaby | RetPally Combat-Rotation](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/paladin/lezbaby_retpally.lua)
- [Masina | Paladin Aura Change](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/paladin/masina_paladin_aura_change.lua)
- [Masina | Paladin Instant Judgement](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/paladin/masina_paladin_judgement.lua)
- [AmsTaFFix | Paladin Retri](https://github.com/AmsTaFFix/gmr-stuff/tree/main/plugins/wotlk/rotations/paladin)
##### Priest
##### Rogue
##### Shaman
- [Lezbaby | Cure Poison Script](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/shaman/lezbaby_cure_poison.lua)
- [Masina | Enhancement Shaman Combat-Rotation](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/shaman/masina_shaman_enhancement.lua)
##### Warlock
- [Samolet | Haunt Chaos Demon](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/warlock/samolet_haunt_chaos_demon.lua)
- [Samolet | Multi-Dot](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/warlock/samolet_multidot.lua)
- [Samolet | Stop Soulfire](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/warlock/samolet_stopsoulfire.lua)
##### Warrior
- [Masina | Bloodsurge Slam Trigger](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/warrior/masina_slam_bloodsurge.lua)
##### ToolBox
- [Lezbaby | Trinket Usage Script](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/rotations/toolbox/lezbaby_trinket_usage.lua)

## Professions
##### Herbalism
- [spwlz | Life Blood](https://gitlab.com/gmr-wow/gmr-community-scripts/-/tree/main/plugins/wotlk/custom/professions/herbalism/life_blood.lua)

# Available Profiles
## Alliance
#### Dungeon
- [extacy | Blood Furnace](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/alliance/dungeon/extacy_blood_furnace_alliance.lua)
#### Gathering
##### Mining
- [extacy | ShadowMoon Valley](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/alliance/gathering/extacy_shadowmoon_valley_alliance_mining.lua)

## Horde
#### Dungeon
- [extacy | Blood Furnace](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/horde/dungeon/extacy_blood_furnace_horde.lua)
- [extacy | Hellfire Ramparts](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/horde/dungeon/extacy_hellfire_ramparts_horde.lua)
- [Muggel | Botanica] (https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/horde/dungeon/Botanica.lua)

#### Grinding
- [Muggel | Crystalsong](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/horde/grinding/78-80_Grind.lua)

#### Gathering
##### Herb + Mining
- [Lezbaby | Terrokar (No Skettis) | Flying](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/horde/gathering/herb%20%2B%20mining/lezbaby_flying_terrokar_herb_mine_noskettis.lua)
##### Mining
- [extacy | ShadowMoon Valley](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/horde/gathering/mining/extacy_shadowmoon_valley_horde_mining.lua)

## Alliance + Horde
##### Mining
- [extacy | Netherstorm](https://gitlab.com/gmr-wow/gmr-community-scripts/-/blob/main/profiles/wotlk/horde%2Balliance/gathering/mining/extacy_netherstorm_mining.lua)
